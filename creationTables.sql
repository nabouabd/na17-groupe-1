DROP TYPE tLocalisation FORCE ;
/
DROP TYPE tPhotos FORCE ;
/
DROP TYPE tDescription FORCE ;
/
DROP TYPE tRubrique FORCE ;
/
DROP TYPE tRubriques FORCE ;
/
DROP TYPE tMotsClés FORCE ;
/
DROP TYPE tAnnonce FORCE ;
/
DROP TYPE tRefAnnonce FORCE ;
/
DROP TYPE tRefsAnnonces FORCE ;
/
DROP TYPE tInscrit FORCE ;
/
DROP TYPE tRefInscrit FORCE ;
/
DROP TYPE tRefsInscrits FORCE ;
/
DROP TYPE tEnchère FORCE ;
/
DROP TYPE tMessage FORCE ;
/
DROP TYPE tTransaction FORCE ;
/
DROP TABLE Inscrit ;
/
DROP TABLE Annonce ;
/
DROP TABLE Enchère ;
/
DROP TABLE Message ;
/
DROP TABLE SauvegardeAnnonce ;
/
DROP TABLE Transa ;
/

-- Création types

CREATE OR REPLACE TYPE tLocalisation AS OBJECT (
  numVoie INTEGER,
  nomVoie VARCHAR2(50),
  codePostal NUMBER(5),
  ville VARCHAR2(50),
  pays VARCHAR2(50),
  infosCompl VARCHAR2(50)
) ;
/

CREATE OR REPLACE TYPE tPhotos AS TABLE OF VARCHAR2(50) ; -- liens vers photos
/

CREATE OR REPLACE TYPE tDescription AS OBJECT (
	photo tPhotos,
	texte VARCHAR2(50)
) ;
/

CREATE OR REPLACE TYPE tMotsClés AS TABLE OF VARCHAR2(50);
/

CREATE OR REPLACE TYPE tInscrit AS OBJECT (
  login VARCHAR2(15) NOT NULL,
  nom VARCHAR2(50) NOT NULL,
  prenom VARCHAR2(50) NOT NULL,
  dateNaissance DATE NOT NULL,
  nbAnnoncesPub INTEGER,
  nbNotesRecues INTEGER,
  solde FLOAT,
  localisation tLocalisation NOT NULL
) ;
/


CREATE OR REPLACE TYPE tRubrique AS OBJECT (
    nom VARCHAR2(50),
    nombreAnnonces INTEGER,
    administrateurs REF tInscrits
) ;
/


CREATE OR REPLACE TYPE tAnnonce AS OBJECT (
    nom VARCHAR2(50),
    refAuteur REF tInscrit,
    motsClés tMotsClés,
    ville VARCHAR2(50),
    datePublication DATE,
    dateFin DATE,
    description1 tDescription,
    rubriques tRubriques,
    prix FLOAT,
    enchère FLOAT,
    typeAnnonce VARCHAR2(20),
    etat CHAR(1)
) ;
/


CREATE OR REPLACE TYPE tRefsAnnonces AS TABLE OF tRefAnnonce ;
/

CREATE OR REPLACE TYPE tEnchère AS OBJECT (
    enchérisseur REF tInscrit,
    refAnnonce REF tAnnonce,
    montant FLOAT,
    dateEnchere DATE
) ;
/

CREATE OR REPLACE TYPE tMessage AS OBJECT (
    loginExp tRefInscrit,
    loginRecept tRefInscrit,
    dateHeureEnvoi DATE,
    état CHAR(1), 
    objet VARCHAR2(50),
    contenu VARCHAR2(50)
) ;
/

CREATE OR REPLACE TYPE tTransaction AS OBJECT (
    protagoniste1 tRefInscrit,
    protagoniste2 tRefInscrit,
    dateTransactionAchat DATE,
    noteProtagoniste1 INTEGER, 
    noteProtagoniste2 INTEGER, 
    annonce tRefAnnonce,
    typeTransaction VARCHAR2(10) 
) ;
/

-- Création tables

CREATE TABLE Inscrit OF tInscrit ;
/

CREATE TABLE Annonce OF tAnnonce(
    CHECK (dateFin > datePublication),
    CHECK (Enchère > prix),
    CHECK (typeAnnonce IN ('offreEnchère', 'offreAchat', 'offreEchange', 'offreDon', 'demandeEchange', 'demandeAchat')),
    CHECK (etat IN ('T', 'F')),
    CHECK (((type = 'offreEnchere') OR (type = 'offreAchatImmediat')) AND (prix IS NOT NULL)),
    CHECK ((NOT((type = 'offreEnchere') OR (type = 'offreAchatImmediat'))) AND (prix IS NULL)),
    CHECK ((type = 'offreEnchere') AND (dateFin IS NOT NULL)),
    CHECK ((NOT(type = 'offreEnchere')) AND (dateFin IS NOT NULL))
);
NESTED TABLE motsClés STORE AS Annonce_motsClés ;
NESTED TABLE rubriques STORE AS Annonce_rubriques ;

CREATE TABLE Enchère OF tEnchère(
    CHECK (Annonce(type)= ’offreEnchere’)
    CHECK (montant > 0) NOT NULL,
);
/

CREATE TABLE Message OF tMessage(
    CHECK (état IN ('T', 'F')),
);
/

CREATE TABLE Transa OF tTransaction(
    CHECK (noteVendeur BETWEEN 0 AND 5),
    CHECK (noteAcheteur BETWEEN 0 AND 5),
    CHECK (typeTransaction IN ('Don', 'Echange', 'Vente'))
);
/

CREATE TABLE SauvegardeAnnonce (
    annonce tRefAnnonce,
    utilisateur tRefInscrit
) ;
/
