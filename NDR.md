# Note de révision

Le présent document vise à répertorier les modifications, suppressions et ajouts effectués dans la NDC depuis le dernier dépôt datant du 21/09/2019.

##### Adéquation avec les termes du cahier des charges (CDC)

Afin d’adopter le vocabulaire du client, donc du CDC :

- Le terme « troc » est remplacé par « échange ».

##### Annonces

- Précision du nombre maximal autorisé (trois) de mots-clés.

- Le terme « annonceur » est remplacé par le terme « inscrit ».

- Ajout des caractéristiques « nombre de réponses » et « nombre de sauvegardes ».

- Une annonce :

- - peut être vue pas zéro, un ou plusieurs inscrits,
  - peut être modifiée et supprimée par zéro à plusieurs inscrits: aucun inscrit, l’inscrit ayant publié l’annonce et/ou un ou plusieurs administrateur(s),
  - fait apparaître une, et une seule, action (don, échange ou vente).

##### Rubriques

- Le nom d’une rubrique sert à l’identifier.
- Une rubrique peut posséder de zéro à plusieurs annonces.

##### Inscrits

- Nous considérons que seuls les inscrits peuvent utiliser la place d’échange. Nous ne gérons donc plus les visiteurs non inscrits, qui sont donc éliminés de la NDC.

- Nous faisons désormais la distinction entre l’ensemble des « **inscrits** » (l’ensemble des utilisateurs de la place d’échange), et ses deux sous-ensembles : « **administrateurs** » d’une part, et « **utilisateurs** » (inscrits non administrateurs) d’autre part.

- Ajout des caractéristiques « nombre d’annonces publiées », « nombre d’inscrits notés », « note moyenne » et « nombre de notes reçues ».

- Elimination du terme « messagerie ». Mais maintien de la gestion des messages.

- Remplacement du terme « actions », qui a une signification bien particulière pour le client et définie dans le CDC, par le terme « manipulations » pour désigner la suppression, la modification, … d’une annonce.

- Suppression de la mention précisant la gestion faite par un inscrit de son propre solde. Si un inscrit recharge son solde lui-même cela signifie que ce n’est pas nous qui le faisons. Donc j’ai supprimé la mention qui précise la gestion faite par un inscrit.

- La note est considérée comme une caractéristique de l’entité « inscrit ». Suppression de la partie « mise en place des notes » (contenu déplacé dans la partie « entité »).

- Le solde du porte-monnaie d’un utilisateur est automatiquement mis à jour après qu’il ait participé à une vente.

- Un inscrit peut :

- - publier, modifier, supprimer, sauvegarder et répondre à zéro, une ou plusieurs annonces,
  - avoir vu de 0 (nouvelle inscription par exemple) à plusieurs annonces,
  - envoyer et recevoir de zéro à plusieurs messages,
  - noter de zéro (s’il n’a participé à aucune transaction) à plusieurs autres inscrits,
  - proposer zéro, une ou plusieurs actions.

##### Localisation

- Le terme « localisation spatiale », initialement dans la NDC, est remplacé par « localisation » avec précision, dans la NDC, que ce dernier correspond au terme « localisation spatiale » mentionné dans le CDC.

- Le numéro de rue peut contenir des “bis”, “ter”, etc…

- Seuls le code postal, la ville et le pays sont obligatoires. Nous laissons à l’inscrit la possibilité de ne pas renseigner son adresse exacte.

- Une localisation peut permettre :

- - de situer une ou plusieurs annonces,
  - de connaître le lieu de résidence d’un ou plusieurs inscrits.

##### Action

- Ajout de caractéristiques d’une action : un numéro qui sert d’identifiant, un initiateur et un répondant.

- La note de l’action est la moyenne des notes que les protagonistes se seront mutuellement attribuées.

- Une action n’a aucune note ni aucune date, temps qu’elle n’a pas été réellement effectuée.

- Une action est soit un échange, soit un don, soit une vente.

- Une vente est, en plus, caractérisée par un montant. Une vente se fait en monnaie virtuelle propre à la plateforme.

- Suppression de la phrase mentionnant la mise à jour du solde par le client.

- Une action peut :

- -  apparaître dans zéro, une ou plusieurs annonces puisqu’on considère que les types d’actions possibles sont prédéfinis et qu’il est possible que certains types n’aient été proposés par aucun inscrit ;
  - être proposée par zéro, un ou plusieurs inscrits.

##### Message

- Un message est, par défaut, non lu.