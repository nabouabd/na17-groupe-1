# Note de clarification

L'objectif du projet de NA17 est de réaliser une place d'échange à l'UTC.

Pour ce faire, nous allons tout d’abord considérer qu’il est possible de réaliser une transaction entre un bien et un service - le terme transaction désignant un don, un troc ou un achat. 

- **La mise en place d’annonces**

La place d’échange sera composée d’annonces, chacune identifiée grâce à un numéro unique. Une **annonce** sera composée d’un *nom*, d’une *localisation spatiale* (détaillée plus bas), ainsi que de zéro, un ou plusieurs *mots-clés*. De plus, une annonce appartiendra à au moins une *rubrique*. Une annonce pourra également être composée d’une *description* (photo et/ou texte), mais cette description ne sera que optionnelle. De plus, une annonce sera composée d’une date de publication ainsi que d’une date de fin de validité (optionnelle). Elle sera également associée à un annonceur, qui sera répertorié grâce à son login. Nous souhaiterions également comptabiliser le *nombre de vues* par annonce. 

Enfin, nous considérons qu’une annonce peut être soit une offre soit une demande, c’est ce que nous appellerons le *type de l’annonce*.. Elle est postée par un seul et unique annonceur et elle peut être sauvegardée par zéro, un ou plusieurs utilisateurs.

Une **rubrique** est caractérisée par son *nom* et le *nombre d’annonces* appartenant à celle-ci.

- **Les différents utilisateurs de la plateforme d’échange**

Nous considérons que les différents visiteurs du site peuvent être des inscrits (administrateurs ou non) ou des invités.

Nous allons identifier les inscrits à la place d’échange de l’UTC grâce à leur login qui sera unique. Un **inscrit** sera également composé d’un *nom*, d’un *prénom*, d’une *date de naissance*, d’une l*ocalisation*, d’une indication sur son *statut* (administrateur ou non) et du *solde de son porte-monnaie*.

Un inscrit peut publier zéro, une ou plusieurs annonces. Il peut aussi les modifier ou les supprimer. Un inscrit peut accéder à sa messagerie pour envoyer et recevoir des messages. 

Ils peuvent aussi noter les utilisateurs avec qui ils ont effectué une transaction et recharger le solde de leur porte-monnaie.

Ce qui différence un simple inscrit d’un **administrateur**, c’est que ce dernier peut publier, modifier, supprimer ou déplacer d’une rubrique à une autre n’importe qu’elle annonce présente sur le site. Alors qu’un inscrit “classique” ne pourra le faire uniquement que sur ses propres annonces. 

Les invités quant à eux peuvent seulement consulter les annonces, et ne possèdent aucune caractéristique.

- **Les ventes**

Les ventes se feront en monnaie virtuelle propre à la plateforme. Nous ne gérerons pas les rechargements par carte bancaire mais les soldes des utilisateurs seront mis à jour après chaque vente. 

- **La mise en place de notes**

Nous avons décidé qu’après chaque transaction effectuée avec succès, les deux utilisateurs qui auront procédé à cette transaction devront noter l’autre protagoniste, selon une note sur 5. Dès qu’un utilisateur a effectué avec succès une transaction et qu’il a évalué l’autre protagoniste, nous calculerons et mettrons à jour sa note moyenne sur 5. 

- **La localisation spatiale**

Une **localisation** **spatiale** est composée d’un *numéro de rue*, du *nom de la voie*, d’un *code postal*, d’une *ville*, d’un *pays* (car on suppose qu’il est possible d’utiliser la plateforme à l’étranger si on est en échange par exemple), ainsi que d’*informations complémentaires* optionnelles. Un inscrit possède une et une seule localisation obligatoire.

- **La messagerie entre inscrits sur la plateforme**

Un **message** sera composé d’un destinataire et d’un expéditeur (on utilisera leur login), d’une heure d’envoi, d’un état (ouvert ou non), d’un objet d’un message obligatoirement, d’un contenu optionnel, et d’une date. Un utilisateur peut envoyer et recevoir 0 ou plusieurs messages.

- **Les transactions**

Chaque **transaction** sera caractérisée par le type d’échange (don, troc ou achat), les 2 protagonistes (identifiés grâce à leur login), dont on précisera le rôle (acheteur, vendeur, donneur, receveur ou troqueur). La transaction sera également composée de la date, du numéro de l’annonce, et de la note qui a été attribuée à chacun des protagonistes. Nous ne gérons pas le lieu de la transaction.

Les visiteurs du site ne peuvent pas effectuer de transaction, il faut obligatoirement être inscrit. Les inscrits peuvent effectuer 0 ou plusieurs transactions. Une transaction est effectuée par 2 inscrits. 