DROP TABLE IF EXISTS AdministrateurRubrique;
DROP TABLE IF EXISTS Annonce;
DROP TABLE IF EXISTS Enchere;
DROP TABLE IF EXISTS Inscrit;
DROP TABLE IF EXISTS Localisation;
DROP TABLE IF EXISTS Message;
DROP TABLE IF EXISTS Rubrique;

CREATE TABLE Localisation (
id INTEGER PRIMARY KEY NOT NULL,
numVoie INTEGER,
nomVoie VARCHAR,
CP INTEGER NOT NULL,
ville VARCHAR NOT NULL,
pays VARCHAR NOT NULL,
infosCompl TEXT
) ;

CREATE TABLE Inscrit (
login VARCHAR(15) PRIMARY KEY NOT NULL,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
dateNaissance DATE NOT NULL,
solde REAL NOT NULL DEFAULT 0,
idLoc INTEGER REFERENCES Localisation(id),
admin BOOLEAN NOT NULL DEFAULT false
) ;

CREATE TABLE Annonce (
num INTEGER PRIMARY KEY NOT NULL,
emetteur VARCHAR(15) REFERENCES Inscrit(login),
repondant VARCHAR(15) REFERENCES Inscrit(login),
noteEmetteur INTEGER CHECK (noteEmetteur>=0 AND noteEmetteur<=5),
noteRepondant INTEGER CHECK (noteRepondant>=0 AND noteRepondant<=5),
nom VARCHAR NOT NULL,
motCle  VARCHAR NOT NULL,
ville VARCHAR NOT NULL,
datePub DATE NOT NULL,
dateFin DATE,
nbVues INTEGER NOT NULL DEFAULT 0,
prix REAL,
prixDebut REAL,
enchereCourante REAL NOT NULL,
type VARCHAR NOT NULL CHECK (type IN (‘offreEnchere’,‘offreDon’,‘offreEchange’, ‘offreAchatImmediat’, ‘demandeAchat’, ‘demandeEchange’)),
etat BOOLEAN NOT NULL DEFAULT true,
lienPhoto VARCHAR,
description TEXT NOT NULL,
CHECK ((type=’offreEnchere’) AND (prixDebut IS NOT NULL) AND (prix IS NULL) AND (dateFin IS NOT NULL)),
CHECK ((type=’offreAchatImmediat’) AND (prix IS NOT NULL) AND (prixDebut IS NULL) AND (enchereCourante IS NULL)),
CHECK (((type=’offreEchange’) OR (type=’offreDon’) OR (type=’demandeAchat’) OR (type=’demandeEchange’)) AND (prixDebut IS NULL) AND (enchereCourante IS NULL) AND (prix IS NULL))
CHECK (emetteur<>repondant)
);

CREATE TABLE Rubrique (
nom VARCHAR PRIMARY KEY NOT NULL,
nbAnnonces INT NOT NULL DEFAULT 0
);

CREATE TABLE Enchere (
encherisseur VARCHAR REFERENCES Inscrit(id),
annonce INTEGER REFERENCES Annonce(num)CHECK(Annonce(type)=’offreEnchere’),
montant REAL NOT NULL,
dateEnchere TIMESTAMP NOT NULL,
PRIMARY KEY(encherisseur, dateEnchere)
);


CREATE TABLE Message (
loginExp VARCHAR(15) REFERENCES Inscrit (login),
loginRecept VARCHAR(15) REFERENCES Inscrit(login),
dateHeureEnvoi TIMESTAMP,
etat BOOLEAN NOT NULL DEFAULT false,
objet VARCHAR(200),
contenu TEXT,
PRIMARY KEY (loginExp,loginRecept,dateHeureEnvoi)
);

CREATE TABLE AdministrateurRubrique (
rubrique VARCHAR REFERENCES Rubrique(nom) NOT NULL,
administrateur VARCHAR REFERENCES Inscrit(login) NOT NULL,
PRIMARY KEY (rubrique,adminstrateur),
CHECK Inscrit(type)=’admin’
);

CREATE OR REPLACE FUNCTION CHECK_ADMIN (inscrit_login VARCHAR)
RETURNS BOOLEAN AS
$$
    SELECT
   	 CASE
   		 WHEN admin IS TRUE THEN true
   		 ELSE false
   	 END
    FROM Inscrit WHERE login = $1;
$$ LANGUAGE 'sql';






CREATE TABLE ReferencementAnnonce(
annonce INTEGER REFERENCES Annonce(num) NOT NULL,
rubrique VARCHAR REFERENCES Rubrique (nom) NOT NULL,
PRIMARY KEY (annonce,rubrique)
);

CREATE TABLE SauvegardeAnnonce (
annonce INTEGER REFERENCES Annonce(num),
login VARCHAR(15) REFERENCES Inscrit(login),
PRIMARY KEY (annonce,login)
);



Données de test :
INSERT INTO Inscrit (login, nom, prenom, dateNaissance, idLoc)
VALUES (‘nico’, ‘Ars’, ‘Nicolas’, 14-03-1995, 1) ;
INSERT INTO Inscrit (login, nom, prenom, dateNaissance, idLoc)
VALUES (‘fredo’, ‘Ars’, ‘Frederic’, 14-03-1995, 1);

INSERT INTO Annonce (num, nom, motCle, ville, datePub, dateFin, prix, enchereCourante, type, lienPhoto, description)
VALUES (1, ‘Velo’,  ‘sport’, ‘Compiegne’, 14-10-2019, 25-10-2019, 25, 30,  ‘offreAchatImmediat’, ‘https://lien_photo.com’, ‘Ce velo, parfait pour la competition, est dotée de jantes aluminum de qualité supérieure’);
Erreur car attribut enchereCourante alors que type=’offreAchatImediat’.
INSERT INTO Annonce (num, nom, motCle, ville, datePub, dateFin, prix, type, lienPhoto, description)
VALUES (1, ‘Velo’,  ‘course’, ‘Compiegne’, 14-10-2019, 25-10-2019,  25, ‘offreAchatImmediat’, ‘https://lien_photo.com’, ‘Ce velo, parfait pour la competition, est dotée de jantes aluminum de qualité supérieure’);
INSERT INTO Annonce (num, nom, motCle, ville, datePub, dateFin, prixDebut, type, lienPhoto, description)
VALUES (2, ‘Peugeot 207’,  ‘citadine’, ‘Bordeaux’, 15-10-2019, 25-10-2019,  3000,  ‘offreEnchere’, ‘https://lien_photo_voiture.com’, ‘Très belle voiture, parfaite pour la ville’);


INSERT INTO ReferencementAnnonce (annonce, rubrique)
VALUES (1, ‘sport’);
INSERT INTO ReferencementAnnonce (annonce, rubrique)
VALUES (2, ‘automobile’);

INSERT INTO Rubrique (nom)
VALUES (‘sport’);
INSERT INTO Rubrique (nom)
VALUES (‘mobilier’);
INSERT INTO Rubrique (nom)
VALUES (‘automobile’);

SELECT ReferencementAnnonce.rubrique, AVG(Annonce.prix)
FROM ReferencementAnnonce, Annonce
WHERE (Annonce.type=’offreAchatImmediat’ AND ReferencementAnnonce.annonce=Annonce.num)
GROUP BY  ReferencementAnnonce.rubrique;
