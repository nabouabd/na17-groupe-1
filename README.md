# Projet NA17 groupe 1

Ce projet contiendra tous les rendus du groupe n°1 : 

- AL SHIKHLEY Liza
- ARIAS Nicolas
- BOUABDALLAH Nawel
- BOULLEE Alicia
- COLLIN Clément



## Rendu n°1

Le rendu n°1 attendu était une note de clarification. Vous pouvez la retrouver sous le nom `NDC.md`. 

## Rendu n°2

Le rendu n°2 attendu était l'UML. Vous pouvez le retrouver sous le nom de `UML_projet.png`et aussi le code sur `code_UML.md`. Nous avons aussi fait des modifications sur la NDC que vous retrouverez sur `NDR.md`.

## Rendu n°2

Le rendu n°3 attendu était le MCD. Vous pouvez le retrouver sous le nom de `MCD_final.pdf`. Nous avons aussi fait une node de révision qui comportent nos modifications de la semaine sur `NDR_2.md`

## Rendu 04/11

Suite à la réunion avec M. Lounis, nous avons modifié notre UML ainsi que notre SQL que vous pouvez trouver sous `UML_modif.png` et `sql_modifie.sql`. Nous avons aussi effectué la normalisation qui se trouve sur `Normalisation.pdf`

## Rendu 18/11

Nous avons fait notre preuve de concept que vous pouvez trouver sous le nom de `annonce_V2.json`