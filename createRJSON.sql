
CREATE TABLE Inscrit(
	login VARCHAR(20) PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	dateNaissance DATE NOT NULL,
	localisation JSON NOT NULL,
	rubriquesEnregistrees JSON
);

INSERT INTO Inscrit VALUES(
	'ariasnic',
	'Nicolas',
	'Ars'
	1999-03-14,
	'{"numeroVoie":27, "nomVoie":"rue des Lilas", "codePostal" : 75014, "ville":"Paris", "pays":"France" }',
	'["Sport", "Mobilier"]'
);


