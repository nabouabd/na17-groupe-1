## Code UML à éxécuter pour le rendu n°2

@startuml
hide circle
class Annonce <<abstract>> {
    numéro : Integer <uniq>
    nom : String
    mot-clé [0..3]: String
    ville : String
    date_publication : Date
    date_fin [0..1] : Date 
    nombre_vues : Integer
    nombre_reponses : Integer
    nombre_sauvegardes : Integer
}

Offre --|> Annonce
Demande --|> Annonce

Class Rubrique {
    nom : String <uniq>
    nombre_annonces : Integer
}

class Description {
    numero_annonce : Integer
    photo [0..1] : String
    texte [0..1] : string
}

class Inscrit {
    login : String <uniq>
    nom : String
    prenom : String
    date_naissance : Date
    nbre_annonces_pub : Integer
    nbre_inscrits_notes : Integer
    nbre_notes : Integer
    solde : Float
    note_moy() : Float
       
}

Administrateur --|> Inscrit

Class Localisation {
    num_rue [0..1] : Integer
    num_voie [0..1] : String
    codePostal : Integer
    ville : String
    pays : String
    infos_compl [0..1] : String
}

class Transaction <<abstract>> {
    numero : Integer <uniq>
    note : Integer [0..5]
    date [0..1] : Date
}

class Don {
    Donneur : String
    Receveur : String
}

class Echange {
    Protagoniste_1 : String
    Protagoniste_2 : String
}

class Achat {
    Acheteur : String
    Vendeur : String
}

Don --|> Transaction
Echange --|> Transaction
Achat --|> Transaction

class Message {
    expediteur : String
    destinataire : String
    date_envoi : Date
    heure_envoi : Time
    etat : String [lu, non lu]
    objet : String
    contenu [0..1] : String
}


Annonce "0..*" -- "1..*" Rubrique : appartenir
Annonce "1" -- "1" Description : décrire
Annonce "0..*" -- "1" Inscrit : publier
Annonce "0..*" -- "0..*" Inscrit : modifier
Annonce "0..*" -- "1" Inscrit : supprimer
Annonce "0..*" -- "0..*" Inscrit : sauvegarder
note "Un inscrit modifie et supprime seulement les annonces qu'IL a publié et sauvegarde les annonces des AUTRES inscrits" as N1
Inscrit .. N1
N1 .. Annonce

Inscrit "1..*" -- "1" Localisation: résider
Inscrit "1" -- "0..*" Message : envoyer
Inscrit "1" -- "0..*" Message : recevoir

Administrateur "1..*" -- "1..*" Rubrique : gérer
Administrateur "1..1" -- "1..*" Annonce : supprimer
Administrateur "1..1" -- "1..*" Annonce : modifier
Administrateur "1..1" -- "1..*" Annonce : déplacer
note "Un administrateur ne peut pas supprimer, déplacer ou modifier une annonce appartenant à une rubrique qu'il ne gère pas" as N2
Administrateur .. N2
N2 .. Annonce

Inscrit "1..1" -- "1..1" Inscrit
    (Inscrit, Inscrit) . Transaction

@enduml