# **Note de révision**

Nous avons opéré quelques modifications sur notre UML : 

- suppression de la classe description (nous l’avons intégrée dans la classe annonce (car les cardinalités entre ces deux classes étaient 1...1)

- suppression des relations “modifier” et “supprimer” entre les classes “Annonce” et “Inscrit” car ces relations n’ont pas de sens dans le modèle conceptuel. On gérera la modification et la suppression au niveau applicatif. 

- modification de la syntaxe dans la classe transaction : nous avions écrit “note : integer [0..5]”. Nous le remplaçons par : “note : integer {0,1,2,3,4,5}” pour que l’utilisateur puisse donner une note entre 0 et 5. 

- La classe Annonce n’est plus abstraite. Donc des annonces peuvent être ni des offres ni des demandes. La classe offre n’est pas une classe abstraite. 

- Nous avons décidé de gérer des enchères. Nous avons créé un héritage. La classe mère est Offre, les classes filles sont : AchatImmédiat et OffreEnchère. Pour les “OffreEnchère”, il y a un montantInitial, une enchèreCourante,

- Ajout d’un attribut etat (disponible ou indisponible) dans la classe annonce, de type booléen. 

- Tant que la transaction n’est pas notée, les soldes des protagonistes ne sont pas mis à jour. 